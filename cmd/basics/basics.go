package main

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
)

func main() {
	basics1()
	basics2()
	basics3()
	basics4()
}

func noop() {}

func append_name(num int) string {
	return "Hello, " + strconv.Itoa(num) + "!"
}

func return_multiple() (thing1 int, thing2 int) {
	return 5, 10
}

func divide_things(n1 int, n2 int) (int, int, error) {
	if n2 == 0 {
		return 0, 0, errors.New("Division by zero error!")
	}
	return n1 / n2, n1 % n2, nil
}

// hello world, variables, if else, switch
func basics1() {
	// "string" may be ommited, because type is inferred
	const v string = "bye, world!"
	// defer fmt.Println(v) // like in zig
	fmt.Println("Hello, world!")

	fmt.Println(len("😀")) // strings may be utf8 but len returns size in bytes

	var inferred_thing1 = "With var"
	inferred_thing2 := "Without var"

	inferred_thing3, inferred_thing4 := "looks", "cool"
	fmt.Println(inferred_thing1, inferred_thing2, inferred_thing3, inferred_thing4)

	fmt.Println(append_name(333))

	v1, v2 := return_multiple()

	res, rem, err := divide_things(5, 10)
	if err != nil {
		panic(err.Error()) // wont happen
	}
	fmt.Printf("5 / 10 = %v, rem: %v\n", res, rem)

	res, rem, err = divide_things(5, 0)
	if err != nil {
		fmt.Println("Intentional error: " + err.Error())
	}

	switch { // sugar over if else (note no variable after switch)
	case 1 == 1:
		noop() // no need for break!
	case 2 == 2:
		noop()
	}

	const a int = 5

	switch a { // based on value of a variable
	case 100:
		noop()
	}

	_ = v1 // like in zig
	_ = v2

}

func basics2() {
	var arr [3]int32 = [...]int32{1, 2, 3} // ... means inferred size (which is 3)
	var arr_addrs [3]*int32
	for i := 0; i < 3; i++ {
		arr_addrs[i] = &arr[i]
	}
	fmt.Println(arr)
	fmt.Println(arr_addrs)

	// unlike rust or zig, slice in go stores
	// ptr + size + cap (like Vec<T> in rust)
	fmt.Printf("Sizeof []int32: %v\n", reflect.TypeOf(([]int32)(nil)).Size())
	var arr_slice []int32 = arr[0:2]
	arr_slice = append(arr_slice, 10)
	fmt.Println(&arr[0], &arr_slice[0], "len/cap:", len(arr_slice), cap(arr_slice)) // same
	arr_slice = append(arr_slice, 10)
	fmt.Println(&arr[0], &arr_slice[0], "len/cap:", len(arr_slice), cap(arr_slice)) // not same!
	// slice gets another memory location at runtime, because
	// it cannot store anymore within append will allocate another chunk

	var what [4]int32 = [...]int32{1, 2, 3, 4}
	arr_slice = append(arr_slice, what[:]...)
	// semantically the same as
	arr_slice = append(arr_slice, what[0], what[1], what[2], what[3])
	fmt.Println(arr_slice)

	// from now on lets use go notation (camelCase)

	// type, len, cap
	// forward allocated 10 elements of int32 (4 * 10 = 40 bytes)
	// of which 5 (20 bytes) are currently initialized by default (0)
	fowrardDefined := make([]int32, 5, 10)
	fmt.Println(fowrardDefined)

	// next, lets do maps

	// maps isnt ordered tho,
	// so each time range mymap would go in different order
	var mymap map[string]int32 = map[string]int32{
		"one":    1,
		"eleven": 11,
		"two":    2,
	}
	fmt.Println(mymap["eleven"])
	// todo: figure out how to handle maybe values
	number, ok := mymap["ten"]
	if ok {
		fmt.Println(number)
	} else {
		fmt.Println("no \"ten\" found")
	}

	for str, num := range mymap {
		fmt.Println(str, num)
	}
}

func basics3() {
	sample := "Hellȯ, wȯrld!"
	fmt.Printf("String: %v\n\tBytes Len: %v\n", sample, len(sample))
	for i, char := range sample { // range merges multiple indecies
		fmt.Printf("sample[%v] = %v\n", i, char)
	}
	for i, char := range []rune(sample) {
		fmt.Printf("[]rune(sample)[%v] = %v\n", i, char)
	}
}

type person struct {
	name string
	age  int32
}

type nameable interface {
	speakToMe()
}

func (p person) speakToMe() {
	fmt.Printf("Hello, %v, aged %v\n", p.name, p.age)
}

func callNameable(n nameable) {
	n.speakToMe()
}

func basics4() {
	willy := person{
		name: "willy",
		age:  100,
	}

	callNameable(willy)
}
